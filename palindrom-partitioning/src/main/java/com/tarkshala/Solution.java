package com.tarkshala;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {

    static Map<String, Boolean> map;
    static Map<String, List<List<String>>> partitionMap;

    public static void main(String[] args) {
        System.out.println(partition("aab"));
    }

    public static List<List<String>> partition(String s) {
        map = new HashMap();
        partitionMap = new HashMap();

        return split(s);
    }

    private static List<List<String>> split(String s) {

        if (s.length() == 0) {
            ArrayList<List<String>> output = new ArrayList<>();
            output.add(new ArrayList<>());
            return output;
        }

        List<List<String>> output = new ArrayList<>();
        for (int i = 1; i <= s.length(); i++) {
            String prefix = s.substring(0, i);
            if (isPalindrom(prefix)) {
                List<List<String>> partitions = split(s.substring(i));
                for (List<String> partition: partitions) {

                    partition.add(0, prefix);
                    output.add(partition);
                }
            }
        }

        return output;
    }

    private static boolean isPalindrom(String s) {

        if (map.containsKey(s)) {
            return map.get(s);
        }

        int i = 0;
        int j = s.length() - 1;
        while(i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                map.put(s, false);
                return false;
            }
            i++;
            j--;
        }

        map.put(s, true);
        return true;
    }
}