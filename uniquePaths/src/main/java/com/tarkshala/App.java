package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        System.out.println(solution.uniquePaths(3, 7));
    }

    static class Solution {

        private int[][] cache;

        private int count(int i, int j, int m, int n) {

            if (cache[i][j] > 0) {
                return cache[i][j];
            }

            int count = 0;

            if (i == m - 1 && j == n - 1) {
                count = 1;
            } else if (i == m - 1) {
                count = count(i, j + 1, m, n);
            } else if (j == n - 1) {
                count = count(i + 1, j, m, n);
            } else {
                count = count(i + 1, j, m, n) + count(i, j + 1, m, n);
            }

            cache[i][j] = count;
            return count;
        }

        public int uniquePaths(int m, int n) {
            this.cache = new int[m][n];
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    cache[i][j] = -1;
                }
            }

            return count(0, 0, m, n);
        }
    }
}
