package com.tarkshala;

import org.junit.Assert;
import org.junit.Test;

public class SolutionTest {

    @Test
    public void testRegex() {
        Assert.assertTrue(App.Solution.isMatch("", ""));
        Assert.assertFalse(App.Solution.isMatch("", "a"));
        Assert.assertTrue(App.Solution.isMatch("a", "a"));
    }
}
