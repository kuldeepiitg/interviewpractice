package com.tarkshala;

public class App {



    public static void main(String[] args) {
        System.out.println(Solution.isMatch("aaaaaaaaaaaaab", "a*a*a*a*a*a*a*a*a*a*c"));
    }

    static class Solution {
        public static boolean isMatch(String s, String p) {

            // termination condition when string is empty
            if (s.length() == 0) {
                if (p.length() == 0) {
                    return true;
                } else if (p.length() == 2 && p.charAt(1) == '*') {
                    return true;
                } else if (p.length() > 2 && p.charAt(1) == '*') {
                    return isMatch(s, p.substring(2));
                }
                return false;
            }

            // termination condition when pattern is empty, string is not
            if (p.length() == 0) {
                return false;
            }

            char firstS = s.charAt(0);
            char firstP = p.charAt(0);

            // first character is a match, then check remaining recursively
            if (firstS == firstP || firstP == '.') {

                if (p.length() == 1) {
                    // pattern is only length 1, then match remaining string with empty
                    return isMatch(s.substring(1), "");
                } else if (p.length() > 1 && p.charAt(1) == '*') {
                    // pattern is having * at second position
                    // first try matching remaining string with full regex, if not matching then try regex starting after *
                    return isMatch(s.substring(1), p)
                            || isMatch(s.substring(1), p.substring(2))
                            || isMatch(s, p.substring(2));
                } else {
                    return isMatch(s.substring(1), p.substring(1));
                }
            } else {
                // first characters are not same
                if (p.length() > 1 && p.charAt(1) == '*') {
                    return isMatch(s, p.substring(2));
                }
            }

            return false;
        }
    }
}
