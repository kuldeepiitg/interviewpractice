package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        System.out.println(solution.climbStairs(3));
    }

    static class Solution {

        private int[] cache;

        public int climbStairs(int n) {
            this.cache = new int[n];
            for (int i = 0; i < n; i++) {
                cache[i] = -1;
            }


            return count(0, n);
        }

        private int count(int m, int n) {
            if (m == n || m == n - 1) {
                return 1;
            }

            if (cache[m] >= 0) {
                return cache[m];
            }

            cache[m] = count(m + 1, n) + count(m + 2, n);
            return cache[m];
        }
    }
}
