package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        int n = solution.numDecodings("226");
        System.out.println(n);
    }

    static class Solution {
        private int[][] cache;

        public int numDecodings(String s) {
            this.cache = new int[s.length() + 1][s.length() + 1];
            for (int i = 0; i < s.length() + 1; i++) {
                for (int j = 0; j < s.length() + 1; j++) {
                    this.cache[i][j] = -1;
                }
            }

            return countWays(0, s.length(), s);
        }

        private int countWays(int i, int j, String input) {

            if (cache[i][j] >= 0) {
                return cache[i][j];
            }

            if (j == i + 1) {
                if (input.charAt(i) == '0') {
                    cache[i][j] = 0;
                } else {
                    cache[i][j] = 1;
                }
            } else if (j == i + 2) {

                if (input.charAt(i) == '0') {
                    cache[i][j] = 0;
                } else {
                    int count = countWays(i, i + 1, input) * countWays(i + 1, j, input);
                    int number = Integer.parseInt(input.substring(i, j));
                    if (10 <= number && number <= 26) {
                        count += 1;
                    }

                    cache[i][j] = count;
                }
            } else {
                int count = 0;
                int number = Integer.parseInt(input.substring(i, i + 1));
                if (0 < number) {
                    count += countWays(i + 1, j, input);
                }

                number = Integer.parseInt(input.substring(i, i + 2));
                if (10 <= number && number <= 26) {
                    count += countWays(i + 2, j, input);
                }
                cache[i][j] = count;
            }
            return cache[i][j];
        }
    }
}
