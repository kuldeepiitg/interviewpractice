package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        int index = solution.search(new int[] {4,5,6,7,0,1,2}, 0);
        System.out.println(index);
    }

    static class Solution {
        public int search(int[] nums, int target) {
            int pivot = findPivot(nums);

            if (pivot == 0) {
                return search(nums, 0, nums.length - 1, target);
            }

            if (nums[0] <= target && target <= nums[pivot-1]) {
                return search(nums, 0, pivot - 1, target);
            } else if (nums[pivot] <= target && target <= nums[nums.length - 1]) {
                return search(nums, pivot, nums.length - 1, target);
            }

            return -1;
        }

        private int search(int[] nums, int start, int end, int target) {

            while (start < end) {
                int mid = (start + end)/2;
                if (nums[mid] == target) {
                    return mid;
                } else if (nums[mid] < target) {
                    start = mid + 1;
                } else if (nums[mid] > target) {
                    end = mid - 1;
                }
            }

            if (nums[start] == target) {
                return start;
            }

            return -1;
        }

        private int findPivot(int[] nums) {
            int start = 0;
            int end = nums.length - 1;

            if (nums[start] <= nums[end]) {
                return 0;
            }

            int movement = 0;
            while (start < end) {
                int mid = (start + end)/2;
                if (nums[start] > nums[mid]) {
                    end = mid - 1;
                    movement = -1;
                } else {
                    start = mid + 1;
                    movement = 1;
                }

                if (nums[start] <= nums[end]) {
                    if (movement == -1) {
                        return end + 1;
                    }

                    if (movement == 1) {
                        return start;
                    }
                }
            }

            throw new RuntimeException("Should always get pivot earlier");
        }
    }
}
