package com.tarkshala;

import java.util.*;

class Solution {

    private static Map<String, Boolean> map;

    public static void main(String[] args) {
        System.out.println(wordBreak("applepenapple", List.of("apple","pen")));
    }

    public static boolean wordBreak(String s, List<String> wordDict) {
        map = new HashMap();
        return wordBreak(s, new HashSet<String>(wordDict));
    }

    public static boolean wordBreak(String s, Set<String> dict) {

        if (s.length() == 0) {
            return true;
        }

        if (map.containsKey(s)) {
            return map.get(s);
        }

        for (int i = 1; i <= s.length(); i++) {

            boolean possible = false;
            String prefix = s.substring(0, i);
            if (dict.contains(prefix)) {
                possible = wordBreak(s.substring(i), dict);
            }

            if (possible) {
                map.put(s, true);
                return true;
            }
        }
        map.put(s, false);
        return false;
    }
}