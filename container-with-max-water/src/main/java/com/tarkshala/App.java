package com.tarkshala;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        int[] heights = new int[]{1,8,6,2,5,4,8,3,7};
        System.out.println(Solution.maxArea(heights));
    }

    public static class Solution {
        public static int maxArea(int[] height) {
            int max = 0;
            List<Tuple> increasingHeights = new ArrayList<Tuple>();
            increasingHeights.add(new Tuple(0, height[0]));

            for (int i = 1; i < height.length; i++) {

                for (Tuple barrier: increasingHeights) {
                    int current = (i - barrier.index) * Math.min(height[i], barrier.height);
                    if (current > max) {
                        max = current;
                    }
                }

                if (height[i] > increasingHeights.get(increasingHeights.size() - 1).height) {
                    increasingHeights.add(new Tuple(i, height[i]));
                }
            }

            return max;
        }

        public static class Tuple {
            public int index;
            public int height;

            public Tuple(int index, int height) {
                this.index = index;
                this.height = height;
            }
        }
    }
}
