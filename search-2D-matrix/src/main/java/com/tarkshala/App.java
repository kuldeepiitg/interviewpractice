package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        int[][] matrix = new int[][]{{1,3,5,7},{10,11,16,20},{23,30,34,60}};
        System.out.println(solution.searchMatrix(matrix, 3));
        System.out.println(solution.searchMatrix(matrix, 11));
        System.out.println(solution.searchMatrix(matrix, 15));
        System.out.println(solution.searchMatrix(matrix, 34));
        System.out.println(solution.searchMatrix(matrix, 65));
    }

    static class Solution {
        public boolean searchMatrix(int[][] matrix, int target) {

            int start = 0;
            int end = matrix.length * matrix[0].length - 1;

            while(start <= end) {
                if (start == end) {
                    int element = get(matrix, start);
                    if (element == target) {
                        return true;
                    } else{
                        return false;
                    }
                }

                int mid = (start + end)/2;
                int element = get(matrix, mid);
                if (target < element) {
                    end = mid - 1;
                } else if (target > element) {
                    start = mid + 1;
                } else {
                    return true;
                }
            }

            return false;
        }

        private int get(int[][] matrix, int place) {
            int rowSize = matrix[0].length;
            int rowIndex = place/rowSize;
            int columnIndex = place%rowSize;

            return matrix[rowIndex][columnIndex];
        }
    }
}
