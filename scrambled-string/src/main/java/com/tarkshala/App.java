package com.tarkshala;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.LinkedList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        Gson gson = new Gson();
        System.out.println( "Hello World!" );
        Solution3 solution = new Solution3();
        System.out.println(solution.isScramble("great", "rgeat"));
    }

    static class Solution {

        Map<String, Set<String>> scrambled;

        public boolean isScramble(String s1, String s2) {

            this.scrambled = new HashMap<>();

            long start = System.currentTimeMillis();
            Set<String> scrambledSet = generateScrambles(s1, s2);
            System.out.println(System.currentTimeMillis() - start);

            return scrambledSet.contains(s2);
        }

        private Set<String> generateScrambles(String s, String t) {

            if (scrambled.containsKey(s)) {
                return scrambled.get(s);
            }

            if (s.length() == 1) {
                Set<String> output = new HashSet();
                output.add(s);
                scrambled.put(s, output);
                return output;
            }

            Set<String> output = new HashSet();
            for (int i = 1; i < s.length(); i++) {
                String left = s.substring(0, i);
                String right = s.substring(i);
                Set<String> lefts = generateScrambles(left, t);
                Set<String> rights = generateScrambles(right, t);
                for (String l: lefts) {
                    for (String r: rights) {
                        output.add(l + r);
                        output.add(r + l);
                    }
                }
            }

            if (output.contains(t)) {
                return output;
            }

            for (String key : output) {
                scrambled.put(key, output);
            }

            return output;
        }
    }

    static class Solution2 {

        Map<String, Set<String>> scrambled;

        public boolean isScramble(String s1, String s2) {

            this.scrambled = new HashMap<>();

            long start = System.currentTimeMillis();
            Set<String> scrambledSet = generateScrambles(s1);
            System.out.println(System.currentTimeMillis() - start);

            return scrambledSet.contains(s2);
        }

        private Set<String> generateScrambles(String s) {

            if (scrambled.containsKey(s)) {
                return scrambled.get(s);
            }

            if (s.length() == 1) {
                Set<String> output = new HashSet();
                output.add(s);
                scrambled.put(s, output);
                return output;
            }

            Set<String> output = new HashSet();
            for (int i = 1; i < s.length(); i++) {
                String left = s.substring(0, i);
                String right = s.substring(i);
                Set<String> lefts = generateScrambles(left);
                Set<String> rights = generateScrambles(right);
                for (String l: lefts) {
                    for (String r: rights) {
                        output.add(l + r);
                        output.add(r + l);
                    }
                }
            }

            scrambled.put(s, output);
            return output;
        }
    }

    static class Solution3 {

        Map<String, Set<String>> scrambled;

        public boolean isScramble(String s1, String s2) {

            this.scrambled = new HashMap();

            for (int i = 1; i <= s1.length(); i++) {
                for (int j = 0; j < s1.length(); j++) {
                    if (j + i <= s1.length()) {
                        String substring = s1.substring(j, j + i);
                        generateScrambles(substring);
                    }
                }
            }

            return scrambled.containsKey(s2);


        }

        private Set<String> generateScrambles(String s) {

            if (scrambled.containsKey(s)) {
                return scrambled.get(s);
            }

            if (s.length() == 1) {
                Set<String> output = new HashSet();
                output.add(s);
                scrambled.put(s, output);
                return output;
            }

            Set<String> output = new HashSet();
            for (int i = 1; i < s.length(); i++) {
                String left = s.substring(0, i);
                String right = s.substring(i, s.length());
                Set<String> lefts = scrambled.get(left);
                Set<String> rights = scrambled.get(right);
                for (String l: lefts) {
                    for (String r: rights) {
                        output.add(l + r);
                        output.add(r + l);
                    }
                }
            }

            for (String key : output) {
                scrambled.put(key, output);
            }

            return output;
        }
    }
}
