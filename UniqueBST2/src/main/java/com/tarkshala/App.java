package com.tarkshala;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        List<TreeNode> output = solution.generateTrees(3);
        System.out.println(output.size());
    }

    static public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    static class Solution {

        private Map<String, List<TreeNode>> cache = new HashMap();

        public List<TreeNode> generateTrees(int n) {
            return generateTrees(1, n);
        }

        private List<TreeNode> generateTrees(int min, int max) {

            String key = min + "_" + max;
            if (cache.containsKey(key)) {
                return cache.get(key);
            }

            if (max - min == 0) {
                List<TreeNode> output = List.of(new TreeNode(min));
                cache.put(key, output);
                return output;
            }

            if (max - min == 1) {
                TreeNode root1 = new TreeNode(min);
                root1.right = new TreeNode(max);

                TreeNode root2 = new TreeNode(max);
                root2.left = new TreeNode(min);

                List<TreeNode> output = List.of(root1, root2);
                cache.put(key, output);
                return output;
            }

            List<TreeNode> output = new ArrayList();
            for (int i = min; i <= max; i++) {
                List<TreeNode> lefts;
                List<TreeNode> rights;
                if (i == min) {
                    rights = generateTrees(i + 1, max);

                    for (TreeNode rightSubtree: rights) {
                        TreeNode root = new TreeNode(min);
                        root.right = rightSubtree;
                        output.add(root);
                    }
                } else if (i == max) {
                    lefts = generateTrees(min, i - 1);

                    for (TreeNode leftSubtree: lefts) {
                        TreeNode root = new TreeNode(max);
                        root.left = leftSubtree;
                        output.add(root);
                    }
                } else {
                    lefts = generateTrees(min, i - 1);
                    rights = generateTrees(i + 1, max);

                    for (TreeNode leftSubtree: lefts) {
                        for (TreeNode rightSubtree: rights) {
                            TreeNode root = new TreeNode(i);
                            root.left = leftSubtree;
                            root.right = rightSubtree;
                            output.add(root);
                        }
                    }
                }
            }
            cache.put(key, output);
            return output;
        }
    }
}
