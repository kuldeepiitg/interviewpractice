package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int trapped = Solution.trap(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1});
        System.out.println(trapped);
    }

    static class Solution {
        public static int trap(int[] height) {
            int[] lefts = new int[height.length];
            int leftMax = 0;
            for (int i = 0; i < height.length; i++) {
                lefts[i] = leftMax;
                if (leftMax < height[i]) {
                    leftMax = height[i];
                }
            }

            int[] rights = new int[height.length];
            int rightMax = 0;
            for (int i = height.length - 1; i >= 0; i--) {
                rights[i] = rightMax;
                if (rightMax < height[i]) {
                    rightMax = height[i];
                }
            }

            int trapped = 0;
            for (int i = 0; i < height.length; i++) {
                trapped += Math.max((Math.min(lefts[i], rights[i]) - height[i]), 0);
            }

            return trapped;
        }
    }
}
