package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println(Solution.threeSumClosest(new int[]{-1,2,1,-4}, 1));
    }

    public class Solution {
        public static int threeSumClosest(int[] nums, int target) {

            int closest = Integer.MAX_VALUE;
            int minDifference = Integer.MAX_VALUE;

            for (int i = 0; i < nums.length; i++) {
                for (int j = i + 1; j < nums.length; j++) {
                    for (int k = j + 1; k < nums.length; k++) {

                        if (Math.abs(nums[i] + nums[j] + nums[k] - target) < minDifference) {
                            minDifference = Math.abs(nums[i] + nums[j] + nums[k] - target);
                            closest = nums[i] + nums[j] + nums[k];
                        }
                    }
                }
            }

            return closest;
        }
    }
}
