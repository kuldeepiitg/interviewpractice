package com.tarkshala;

public class App {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.longestValidParentheses("((((()())()()))()(()))"));;
    }

    static class Solution {

        private int[][] validParentheses;
        private int longest = 0;

        public int longestValidParentheses(String s) {
            this.validParentheses = new int[s.length()][s.length()];

            if (s.length() == 0 || s.length() == 1) {
                return 0;
            }

            isValid(s, 0, s.length() - 1);

            return longest;
        }

        private boolean isValid(String s, int start, int end) {
            if (validParentheses[start][end] == 1) {
                if (longest < end - start + 1) {
                    longest = end - start + 1;
                }
                return true;
            } else if (validParentheses[start][end] == -1) {
                return false;
            }

            if (end == start) {
                validParentheses[start][end] = -1;
                return false;
            }

            if (end == start + 1) {
                if (s.charAt(start) == '(' && s.charAt(end) == ')') {
                    validParentheses[start][end] = 1;
                    if (longest < end - start + 1) {
                        longest = end - start + 1;
                    }
                    return true;
                } else {
                    validParentheses[start][end] = -1;
                    return false;
                }
            }

            isValid(s, start, end-1);
            isValid(s, start+1, end);
            isValid(s, start+1, end-1);
            isValid(s, start+2, end);
            isValid(s, start, end-2);

            // even
            if ((end - start) % 2 == 1) {
                if ((s.charAt(start) == '(' && s.charAt(end) == ')' && validParentheses[start+1][end-1] == 1)
                        ||(s.charAt(start) == '(' && s.charAt(start+1) == ')' && validParentheses[start+2][end] == 1)
                        ||(s.charAt(end-1) == '(' && s.charAt(end) == ')' && validParentheses[start][end-2] == 1)) {

                    validParentheses[start][end] = 1;
                    if (longest < end - start + 1) {
                        longest = end - start + 1;
                    }
                    return true;
                }
            } else {
                // odd
                validParentheses[start][end] = -1;
                return false;
            }

            validParentheses[start][end] = -1;
            return false;
        }


    }

}
