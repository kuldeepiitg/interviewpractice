package com.tarkshala;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws FileNotFoundException {

        File file = new File("./output.txt");
        Scanner scanner = new Scanner(file);
        ArrayList<Integer> list = new ArrayList<>();
        while (scanner.hasNextInt()) {
            list.add(scanner.nextInt());
        }
        int[] input = list.stream().mapToInt(Integer::intValue).toArray();

        System.out.println(System.currentTimeMillis());
        int sum = Solution.maxSubArray(input);
        System.out.println(System.currentTimeMillis());
        System.out.println(sum);

        System.out.println(System.currentTimeMillis());
        sum = SolutionIterative.maxSubArray(input);
        System.out.println(System.currentTimeMillis());

        System.out.println(sum);
        /*
        [-2, 1,-3, 4,-1, 2, 1,-5, 4]
         */


    }

    static class Solution {
        public static int maxSubArray(int[] nums) {

            if (nums.length == 0) {
                return 0;
            } else if (nums.length == 1) {
                return nums[0];
            }

            int[][] cache = new int[nums.length + 1][nums.length + 1];
            boolean[][] valid = new boolean[nums.length + 1][nums.length + 1];

            int max = Integer.MIN_VALUE;
            for(int i = 1; i <= nums.length; i++) {
                for(int j = 0; j <= nums.length - i; j++) {

                    if (i == 1) {
                        cache[j][j+i] = nums[j];
                        valid[j][j+i] = true;
                        if (max < cache[j][j+i]) {
                            max = cache[j][j+i];
                        }
                        continue;
                    }

                    cache[j][j+i] = cache[j][j+i - 1] + nums[j+i - 1];
                    valid[j][j+i] = true;

                    if (max < cache[j][j+i]) {
                        max = cache[j][j+i];
                    }
                }
            }

            return max;
        }
    }

    static class SolutionIterative {
        public static int maxSubArray(int[] nums) {

            if (nums.length == 0) {
                return 0;
            } else if (nums.length == 1) {
                return nums[0];
            }

            int[] lefts = new int[nums.length];
            int leftSum = 0;
            for (int i = 0; i < nums.length; i++) {
                lefts[i] = leftSum;
                leftSum += nums[i];
            }

            int total = leftSum;

            int[] rights = new int[nums.length];
            int rightSum = 0;
            for (int i = nums.length - 1; i >= 0; i--) {
                rights[i] = rightSum;
                rightSum += nums[i];
            }

//            Gson gson = new Gson();
//            System.out.println();
//            System.out.println(gson.toJson(nums));
//            System.out.println(gson.toJson(lefts));
//            System.out.println(gson.toJson(rights));
//            System.out.println(total);
//            System.out.println();

            int max = Integer.MIN_VALUE;
            for (int i = 0; i < nums.length; i++) {
                for (int j = i; j < nums.length; j++) {
                    if (max < total - (lefts[i] + rights[j])) {
                        max = total - (lefts[i] + rights[j]);
                    }
                }
            }

            return max;
        }
    }
}
