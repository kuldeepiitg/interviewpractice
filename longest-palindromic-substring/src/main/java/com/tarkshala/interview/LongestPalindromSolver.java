package com.tarkshala.interview;

import java.util.LinkedList;

class LongestPalindromSolver {
    public String longestPalindrome(String s) {
        Interval largest = new Interval(0, 0);

        LinkedList<Interval> palindroms = new LinkedList();
        // add single char palindroms
        for (int i = 0; i < s.length(); i++) {
            palindroms.add(new Interval(i, i));
        }

        // add double char palindroms
        for (int i = 0; i < s.length() - 1; i++) {
            if (s.charAt(i) == s.charAt(i+1)) {
                palindroms.add(new Interval(i, i+1));
                if (i + 1 - i + 1 > largest.end - largest.start + 1) {
                    largest = new Interval(i, i+1);
                }
            }
        }

        // For each palindrom, keep checking neighboring chars to get bigger palindroms
        while(!palindroms.isEmpty()) {
            Interval palindrom = palindroms.poll();
            int start = palindrom.start - 1;
            int end = palindrom.end + 1;

            if (start >= 0 && end < s.length() && s.charAt(start) == s.charAt(end)) {
                palindroms.add(new Interval(start, end));
                if (end - start + 1 > largest.end - largest.start + 1) {
                    largest = new Interval(start, end);
                }
            }
        }

        return s.substring(largest.start, largest.end + 1);
    }

    public class Interval {
        public int start;
        public int end;

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
}