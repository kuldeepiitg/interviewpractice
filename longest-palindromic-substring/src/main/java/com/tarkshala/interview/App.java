package com.tarkshala.interview;

public class App {
    public static void main(String[] args) {
        LongestPalindromSolver solver = new LongestPalindromSolver();
        String palindrome = solver.longestPalindrome("babad");
        System.out.println(palindrome);
    }
}
