package com.tarkshala;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws JsonProcessingException {
        List output = Solution.threeSum(new int[]{-1,0,1,2,-1,-4});
        System.out.println(new ObjectMapper().writeValueAsString(output));
    }

    class Solution {
        public static List<List<Integer>> threeSum(int[] nums) {

            List<List<Integer>> output = new ArrayList<>();
            Set<String> alreadyPresent = new HashSet();

            Map<Integer, Integer> lookupMap = new HashMap();
            for (int k = 0; k < nums.length; k++) {
                lookupMap.put(nums[k], k);
            }


            for (int i = 0; i < nums.length; i++) {
                for (int j = i + 1; j < nums.length; j++) {

                    int tuppleSum = nums[i] + nums[j];
                    if (lookupMap.containsKey(-tuppleSum) && lookupMap.get(-tuppleSum) > j) {
                        int k = lookupMap.get(-tuppleSum);
                        ArrayList<Integer> tripplet = new ArrayList();
                        tripplet.add(nums[i]);
                        tripplet.add(nums[j]);
                        tripplet.add(nums[k]);

                        Collections.sort(tripplet);

                        String code = getEncoding(tripplet);
                        if (!alreadyPresent.contains(code)) {
                            output.add(tripplet);
                            alreadyPresent.add(code);
                        }
                    }

                }
            }

            return output;
        }

        private static String getEncoding(List<Integer> tripplet) {
            return tripplet.get(0) + "," + tripplet.get(1) + "," + tripplet.get(2);
        }
    }
}
