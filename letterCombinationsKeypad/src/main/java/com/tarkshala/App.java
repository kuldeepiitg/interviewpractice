package com.tarkshala;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        List<String> list = Solution.letterCombinations("23");
        for (String item: list) {
            System.out.println(item);
        }
    }

    public static class Solution {
        public static List<String> letterCombinations(String digits) {
            Map<Integer, List<String>> keypad = new HashMap();
            keypad.put(1, List.of());
            keypad.put(2, List.of("a", "b", "c"));
            keypad.put(3, List.of("d", "e", "f"));
            keypad.put(4, List.of("g", "h", "i"));
            keypad.put(5, List.of("j", "k", "l"));
            keypad.put(6, List.of("m", "n", "o"));
            keypad.put(7, List.of("p", "q", "r", "s"));
            keypad.put(8, List.of("t", "u", "v"));
            keypad.put(9, List.of("w", "x", "y", "z"));
            keypad.put(0, List.of());

            return getCombinations(digits, keypad);
        }

        private static List<String> getCombinations(String digits, Map<Integer, List<String>> keypad) {
            if (digits.length() == 0) {
                return List.of();
            } else if (digits.length() == 1) {
                return keypad.get(Integer.valueOf(digits));
            }

            List<String> children = getCombinations(digits.substring(1), keypad);
            List<String> keyChars = keypad.get(Integer.valueOf(digits.substring(0, 1)));
            ArrayList<String> output = new ArrayList();
            for (String keyChar : keyChars) {
                for (String child: children) {
                    output.add(keyChar + child);
                }
            }

            return output;
        }
    }
}
