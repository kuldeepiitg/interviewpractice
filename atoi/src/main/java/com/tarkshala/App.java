package com.tarkshala;

import java.util.Set;

public class App {
    public static void main(String[] args) {
        System.out.println(Solution.myAtoi("-2147483648"));
    }

    class Solution {
        public static int myAtoi(String s) {
            char[] input = s.toCharArray();
            Set<Character> digits = Set.of('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

            long number = 0;
            boolean negative = false;

            int i = 0;
            for (; i < input.length && input[i] == ' '; i++) {
                System.out.println("Leading Space at " + i);
            }

            for (; i < input.length && (input[i] == '+' || input[i] == '-'); i++) {
                System.out.println("Sign encountered" + input[i] + " at " + i);
                if (input[i] == '-') {
                    negative = true;
                }
                i++;
                break;
            }

            for (; i < input.length && digits.contains(input[i]); i++) {
                System.out.println("Digit found: " + input[i] + " at " + i);
                number *= 10;
                number += Integer.valueOf(String.valueOf(input[i]));

                if (negative && -1 * number < Integer.MIN_VALUE) {
                    return Integer.MIN_VALUE;
                }

                if (!negative && number > Integer.MAX_VALUE) {
                    return Integer.MAX_VALUE;
                }
            }

            if (negative) {
                number *= -1;
            }

            return (int)number;
        }
    }
}
