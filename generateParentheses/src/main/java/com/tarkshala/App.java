package com.tarkshala;

import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println(Solution.generateParenthesis(5));
    }

    class Solution {

        private static Map<Integer, Set<String>> cached = new HashMap();

        public static List<String> generateParenthesis(int n) {

            return new ArrayList(generateParenthesisSet(n));
        }

        private static Set<String> generateParenthesisSet(int n) {

            if (n ==0) {
                return Set.of("");
            }

            if (n == 1) {
                return Set.of("()");
            }

            if (cached.containsKey(n)) {
                return cached.get(n);
            }

            Set<String> output = new HashSet<String>();

            for (int i = 0; i <= n-1; i++) {
                Set<String> leftSet = generateParenthesisSet(i);
                Set<String> rightSet = generateParenthesisSet(n-1 - i);

                for (String left: leftSet) {
                    for (String right: rightSet) {
                        output.add("()" + left + right);
                        output.add("(" + left + ")" + right);
                        output.add(left + "()" + right);
                        output.add(left +"(" + right + ")");
                        output.add(left + right + "()");
                        output.add("(" + left + right + ")");
                    }
                }
            }

            cached.put(n, output);
            return output;
        }
    }

}
