package com.tarkshala;

import java.util.LinkedList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println(Solution.jump(new int[]{2,3,1,1,4}));
    }

    static class Solution {

        public static int jump(int[] nums) {

            if (nums.length == 0 || nums.length == 1) {
                return 0;
            }

            LinkedList<Integer> queue = new LinkedList();
            boolean[] visited = new boolean[nums.length];
            int[] steps = new int[nums.length];

            queue.add(0);
            steps[0] = 0;
            visited[0] = true;

            int min = Integer.MIN_VALUE;

            while(!queue.isEmpty()) {
                int place = queue.poll();
                for(int i = 1; i <= nums[place]; i++) {
                    int nextPosition = place + i;

                    if (!visited[nextPosition]) {
                        queue.add(nextPosition);
                        steps[nextPosition] = steps[place] + 1;
                        visited[nextPosition] = true;

                        if (nextPosition == nums.length - 1) {
                            return steps[nextPosition];
                        }
                    }
                }
            }

            return steps[nums.length - 1];
        }
    }
}
