package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        int min = solution.minPathSum(new int[][]{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}});
        System.out.println(min);
    }

    static class Solution {

        private int[][] cache;

        public int minPathSum(int[][] grid) {
            this.cache = new int[grid.length][grid[0].length];
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length; j++) {
                    this.cache[i][j] = -1;
                }
            }

            return minSum(0, 0, grid);
        }



        private int minSum(int i, int j, int[][]grid) {

            if (cache[i][j] >= 0) {
                return cache[i][j];
            }

            int sum = 0;

            if (i == grid.length - 1 && j == grid[0].length - 1) {
                sum = grid[i][j];
            } else if (i == grid.length - 1) {
                sum = grid[i][j] + minSum(i, j + 1, grid);
            } else if (j == grid[0].length - 1) {
                sum = grid[i][j] + minSum(i + 1, j, grid);
            } else {
                sum = grid[i][j] + Math.min(minSum(i, j + 1, grid), minSum(i + 1, j, grid));
            }

            cache[i][j] = sum;

            return sum;
        }
    }
}
