package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println(Solution.reverse(123));
        System.out.println(Solution.reverse(-1563847412));
    }

    class Solution {
        public static int reverse(int x) {

            if (x == 0) {
                return 0;
            }

            boolean negative = x < 0;
            if (negative) {
                x *= -1;
            }

            long number = 0;
            while (x > 0) {
                int digit = x%10;
                x /= 10;
                number *= 10;
                number += digit;
            }

            if (negative) {
                number *= -1;
            }

            if (number > Integer.MAX_VALUE || number < Integer.MIN_VALUE) {
                return 0;
            }

            return (int) number;
        }
    }
}
