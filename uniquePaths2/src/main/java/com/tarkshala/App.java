package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Solution solution = new Solution();
        System.out.println(solution.uniquePathsWithObstacles(new int[][]{{0,0,0},{0,1,0},{0,0,0}}));
    }

    static class Solution {
        public int uniquePathsWithObstacles(int[][] obstacleGrid) {

            int m = obstacleGrid.length;
            int n = obstacleGrid[0].length;

            return uniquePaths(m, n, obstacleGrid);
        }

        private int[][] cache;

        private int count(int i, int j, int m, int n, int[][] obstacleGrid) {

            if (obstacleGrid[i][j] == 1) {
                return 0;
            }

            if (cache[i][j] > 0) {
                return cache[i][j];
            }

            int count = 0;

            if (i == m - 1 && j == n - 1) {
                count = 1;
            } else if (i == m - 1) {
                count = (obstacleGrid[i][j + 1] == 0 ? count(i, j + 1, m, n, obstacleGrid) : 0);
            } else if (j == n - 1) {
                count = (obstacleGrid[i + 1][j] == 0 ? count(i + 1, j, m, n, obstacleGrid) : 0);
            } else {
                count = (obstacleGrid[i][j + 1] == 0 ? count(i, j + 1, m, n, obstacleGrid) : 0) + (obstacleGrid[i + 1][j] == 0 ? count(i + 1, j, m, n, obstacleGrid) : 0);
            }

            cache[i][j] = count;
            return count;
        }

        public int uniquePaths(int m, int n, int[][] obstacleGrid) {
            this.cache = new int[m][n];
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    cache[i][j] = -1;
                }
            }

            return count(0, 0, m, n, obstacleGrid);
        }
    }
}
