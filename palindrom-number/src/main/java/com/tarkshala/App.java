package com.tarkshala;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println(Solution.isPalindrome(12321));
    }

    public class Solution {
        public static boolean isPalindrome(int x) {
            int original = x;
            int reverse = 0;
            while(x > 0) {
                reverse *= 10;
                reverse += x%10;
                x /= 10;
            }

            if (original == reverse) {
                return true;
            }
            return false;
        }
    }
}
